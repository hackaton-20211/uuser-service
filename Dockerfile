FROM openjdk:17
EXPOSE 9001
COPY target/user-service-2.6.1.jar /user-service-2.6.1.jar
ENTRYPOINT ["java","-jar","/user-service-2.6.1.jar"]