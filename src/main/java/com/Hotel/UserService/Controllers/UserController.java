package com.Hotel.UserService.Controllers;

import com.Hotel.UserService.Entities.User;
import com.Hotel.UserService.Services.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userservice;

    @GetMapping("/hello")
    public String sayHello() {
        return String.format("Hello World!");
    }

    @PostMapping("/save/{user}")
    public User saveTest(@PathVariable(value = "user") User value) {
        log.info("User saved");
        return userservice.saveUser(value);
    }

    @GetMapping("/{user}")
    public User getUser(@PathVariable(value = "user") String value) {
        log.info("User requested" + value);
        return userservice.getUser(value);
    }

    @DeleteMapping("/delete/{user}")
    public void deleteUser(@PathVariable(value = "user") String value) {
        log.info("User deleted" + value);
        userservice.deleteUser(value);
    }


}
