package com.Hotel.UserService.Entities;

import lombok.*;

import javax.persistence.*;
import java.sql.Date;

@Table(name = "users")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private String surnames;

    @Column
    private String email;

    @Column
    private Date birthday;

    @Column
    private int phone;

    @Column
    private String direction;

}
